import serial
import sys
import datetime
import glob
import csv


def findUSB():
    print("look for all available USB ports")
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    USBports = []

    for port in ports:
        try:
            s = serial.Serial(port, 9600, timeout=1)
            s.close()
            USBports.append((int(''.join(i for i in port if i.isdigit())), s.portstr))
        except (OSError, serial.SerialException):
            pass

    return USBports

def getport(USBportslist, No):
    port = [item[1] for item in USBportslist if item[0] == No]
    return str(port[0])


USBports = findUSB()

for n,s in USBports:
    print("(%d) %s" % (n,s))
selection = int(input("Enter port number:"))

USBport = getport(USBports, selection)
print("Connecting to: " + USBport)

try:
    ser = serial.Serial(USBport, 9600, timeout=1)
    ser.flushInput()
    print("connected to: " + ser.portstr)
    connected = True
    with open("MiniSunScan_data.csv", "a") as f:
        writer = csv.writer(f, delimiter=",")
        writer.writerow(["Time","Lipo","A0-R","A0-PAR","A1-R","A1-PAR","A2-R","A2-PAR","A3-R","A3-PAR","A4-R","A4-PAR","A5-R","A5-PAR","MeanRAR","MeanPAR","Take","Send"])
except serial.SerialException:
    print("Failed to connect")
    connected = False
    pass


while connected:
    try:
        ser_bytes = ser.readline()
        decoded_bytes = ser_bytes[0:len(ser_bytes)-2].decode("utf-8")
        SCvals = decoded_bytes.split(",")
        print(SCvals)
        with open("MiniSunScan_data.csv", "a") as f:
            writer = csv.writer(f, delimiter=",")
            Row = [datetime.datetime.now()]
            for i in SCvals:
                Row.append(i)
            writer.writerow(Row)
    except:
        print("Stopping")
        break
