#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <bluefruit.h>

BLEDis bledis;
BLEHidAdafruit blehid;

#define Version "v2.1"
#define Name "Light_Intercepta_0"

#include <Adafruit_NeoPixel.h>
#define NEO  8
#define NeoPIXELS 1
Adafruit_NeoPixel pixels(NeoPIXELS, NEO, NEO_GRB + NEO_KHZ800);

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 32 // OLED display height, in pixels
#define OLED_RESET -1
#define buttonTake 12 
#define buttonSend 11
#define beep 6
#define anoPins 6 

#define  PIN_VBAT A6
uint32_t vbat_pin = PIN_VBAT;             // A7 for feather nRF52832, A6 for nRF52840
#define VBAT_MV_PER_LSB   (0.73242188F)   // 3.0V ADC range and 12-bit ADC resolution = 3000mV/4096
#ifdef NRF52840_XXAA    // if this is for nrf52840
#define VBAT_DIVIDER      (0.5F)               // 150K + 150K voltage divider on VBAT
#define VBAT_DIVIDER_COMP (2.0F)          // Compensation factor for the VBAT divider
#else
#define VBAT_DIVIDER      (0.71275837F)   // 2M + 0.806M voltage divider on VBAT = (2M / (0.806M + 2M))
#define VBAT_DIVIDER_COMP (1.403F)        // Compensation factor for the VBAT divider
#endif
#define REAL_VBAT_MV_PER_LSB (VBAT_DIVIDER_COMP * VBAT_MV_PER_LSB)

Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

bool takebtn = false;
bool sendbtn = false;

int lightpins[anoPins] = {A5, A4, A3, A2, A1, A0};
unsigned long lightVals[anoPins];
unsigned long lightMeans[anoPins];
int LightP;

int Count = 0;

bool verbosemode = true;
bool sendall = false;
bool slowmode = false;
int nomean = 6;
char *LEs[] = {"", " ", ",", "\t", "\n"};
int LEuse = 3;

const unsigned char Sun [] PROGMEM = {
  0x00, 0x7f, 0xfc, 0x1f, 0x7f, 0xfc, 0x1f, 0x7f, 0xfc, 0x02, 0x7f, 0xfc, 0x00, 0x3f, 0xfc, 0x01, 
  0xbf, 0xfc, 0x03, 0x9f, 0xfc, 0x07, 0x9f, 0xfc, 0x0e, 0x0f, 0xfc, 0x00, 0x27, 0xfc, 0x00, 0x73, 
  0xfc, 0x00, 0x70, 0xfc, 0x00, 0xe0, 0x3c, 0x01, 0x87, 0x00, 0x00, 0x07, 0x18, 0x00, 0x06, 0x18, 
  0x00, 0x04, 0x18, 0x00, 0x08, 0x18, 0x00, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00
};





void setup() {
    Serial.begin(9600);

    for(int i=0; i<sizeof(lightpins); i++){
      pinMode(lightpins[i], INPUT);
    }
    
    pinMode(buttonTake, INPUT_PULLUP);
    pinMode(buttonSend, INPUT_PULLUP);
    pinMode(beep, OUTPUT);

    // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
    if(!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x32
      Serial.println(F("SSD1306 allocation failed"));
      for(;;); // Don't proceed, loop forever
    }

    readVBAT();
    
    // Clear the buffer
    display.clearDisplay();
  
    pixels.begin();
    NeoSet(true, 40, 20, 0);
    
    StartText();
    Serial.println("Start");
    NeoSet(true, 80, 40, 20);

    // from adafruit
    Bluefruit.begin();
    Bluefruit.setTxPower(4);    // Check bluefruit.h for supported values
    Bluefruit.setName(Name);

    NeoSet(true, 160, 80, 40);
    // Configure and Start Device Information Service
    bledis.setManufacturer("IBERS");
    bledis.setModel("Light Intercepta");
    bledis.begin();
    /* Start BLE HID
   * Note: Apple requires BLE device must have min connection interval >= 20m
   * ( The smaller the connection interval the faster we could send data).
   * However for HID and MIDI device, Apple could accept min connection interval 
   * up to 11.25 ms. Therefore BLEHidAdafruit::begin() will try to set the min and max
   * connection interval to 11.25  ms and 15 ms respectively for best performance.
   */
    NeoSet(true, 0, 160, 80);
    blehid.begin();

    // Set callback for set LED from central
    blehid.setKeyboardLedCallback(set_keyboard_led);
  
    /* Set connection interval (min, max) to your perferred value.
     * Note: It is already set by BLEHidAdafruit::begin() to 11.25ms - 15ms
     * min = 9*1.25=11.25 ms, max = 12*1.25= 15 ms 
     */
    /* Bluefruit.Periph.setConnInterval(9, 12); */

    NeoSet(true, 0, 0, 160);
    // Set up and start advertising
    startAdv();
    // end from adafruit


    Sound(40, 220/2);
    Sound(90, 220);
    Sound(40, 220/2);
    Sound(90, 220*2);
    Sound(40, 220);
    Sound(90, 220*3);
    Sound(40, 220*2);
    Sound(90, 220*4);
    Sound(40, 220*3);
    Sound(90, 220*5);
    Sound(40, 220*4);
    Sound(90, 220*6);
    
    NeoSet(true, 200, 0, 0);
    
    if(verbosemode) {
      Serial.println("Time,Lipo,A0-R,A0-PAR,A1-R,A1-PAR,A2-R,A2-PAR,A3-R,A3-PAR,A4-R,A4-PAR,A5-R,A5-PAR,MeanRAR,MeanPAR,Take,Send");
    } else {
      Serial.println("Start");
    }
    NeoSet(false, 0,0,0);
}



void loop() {
  float vbat_mv = readVBAT();
  // Convert from raw mv to percentage (based on LIPO chemistry)
  uint8_t vbat_per = mvToPercent(vbat_mv);
  // Display the results
  if(verbosemode) {
    Serial.print("HMSms,");
    Serial.print(vbat_mv);
    Serial.print("mV (");
    Serial.print(vbat_per);
    Serial.print("%),");
  }
  
  drawSun();
  GoDot();
  takebtn = Button(buttonTake);
  sendbtn = Button(buttonSend);

  if(verbosemode) {
    Serial.print(",T:");
    Serial.print(takebtn);
    Serial.print(",S:");
    Serial.print(sendbtn);
  }

  if(takebtn) {
    NeoSet(true, 255,255,255);
    Sound(15, 220*5);
    LightP = MeanLight();
    Sound(10, 180*5);
    SendText("Taken");
    HelpText("Send Measurement?");
    BatLeval(vbat_per);
    sendbtn = Button(buttonSend);
    Serial.print("RAR=");
    Serial.print(LightP);
    Serial.print(" : PAR=");
    Serial.println(LighttoPAR(LightP));
    delay(700);
    NeoSet(false, 0,0,0);

    while(sendbtn == false) {
      SendText(String(LighttoPAR(LightP)));
      HelpText("Send or re-take?");
      BatLeval(vbat_per);
      delay(10);
      sendbtn = Button(buttonSend);
      takebtn = Button(buttonTake);
      if(takebtn){break;}
    }
    
    if(sendbtn) {
      Sound(10, 220*2);
      delay(100);
      if(sendall) {
        for(int i=0; i<anoPins; i++){
          SendBT(String(LighttoPAR((lightMeans[i]/Count)), DEC));
        }
      } else {
        SendBT(String(LighttoPAR(LightP), DEC));
      }
      delay(100);
    }

    if(takebtn) {
      SendText("Take again");
      HelpText("Not Sent?");
      BatLeval(vbat_per);
      delay(500);
    }
  }
  
  if(verbosemode) {
    Serial.println("");
  }  

  if(slowmode) {
    delay(30000);  
  }
  delay(20);  
}


int getLight(int pin) {
  analogReadResolution(12);
  int Light = analogRead(pin);
  unsigned int LightAvg = 0;
  byte i;
 
  //Take 5 readings and average them
  Light = analogRead(pin);
  for(i=0; i<3; i++) {    
    delay(4);                   
    Light = analogRead(pin);
    LightAvg += Light;
  }
    
  Light = LightAvg / 5;
  //Serial.println(Light);
  delay(6);   //A delay here stops the button press from running into the next option
  return Light;
}


int MeanLight(void) {
  int total = 0;

  while(Count != nomean) {
     drawSun();
     SendText("Taking");
     delay(10);
  }

  Serial.println("");
  Serial.println("TAKEN");
  if(verbosemode) {
      Serial.print(" {");
      Serial.print(Count);
      Serial.println("} ");
  }
  unsigned long lightTotals[anoPins];
  for(int i = 0; i < anoPins; ++i){
    if(verbosemode) {
      Serial.print(i);
      Serial.print(" = ");
      Serial.print(lightVals[i]);
      Serial.print(":");
      Serial.print(lightMeans[i]);
      Serial.print(":");
      Serial.print(lightMeans[i]/Count);
    }
    lightTotals[i] = lightMeans[i]/Count;
  }
  
  int AvrLight = Avr(lightTotals, anoPins);
  return AvrLight;
}


int Avr (unsigned long * array, int len)   {
  long sum = 0L;
  
  for (int i = 0; i < len ; i++) {
    sum += array[i];
  }
  
  float AvL = ((float) sum) / len;
  int out = (int)(AvL+0.5f);
  return out;
}


void StartText(void) {
  display.clearDisplay();
  display.clearDisplay();

  display.setTextSize(2); // Draw 2X-scale text
  display.setTextColor(WHITE);
  display.setCursor(30, 4);
  display.println(F("Light"));
  display.setCursor(3, 18);
  display.println(F("Intercepta"));
  display.drawBitmap(106, 0,  Sun, 22, 22, WHITE);
  display.display();      // Show initial text
  delay(100);
}


void SendText(String txt) {
  display.clearDisplay();
  display.clearDisplay();

  display.setTextSize(2); // Draw 2X-scale text
  display.setTextColor(WHITE);
  display.setCursor(2, 2);
  display.println(txt);
  display.display();
  delay(25);
}


void SendBT(String DatasT) {
  char Datachar[5]; 

  NeoSet(true, 10, 10, 250);
  DatasT.toCharArray(Datachar,5);
   
  blehid.keyPress(Datachar[0]);
  Sound(22, 220*3);
  blehid.keyRelease();
  
  blehid.keyPress(Datachar[1]);
  Sound(22, 220*3);
  blehid.keyRelease();
  
  blehid.keyPress(Datachar[2]);
  Sound(22, 220*3);
  blehid.keyRelease();
  
  blehid.keyPress(Datachar[3]);
  Sound(22, 220*3);
  blehid.keyRelease();
  
  blehid.keyPress(*LEs[LEuse]);
  Sound(22, 220*3);
  blehid.keyRelease();
  
  NeoSet(false, 0, 0, 0);
  //Serial.println(*Datachar);
  
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(BLACK);
  for(int16_t i=0; i<max(display.width(),display.height())/2; i+=2) {
    display.drawCircle(display.width()/2, display.height()/2, i, WHITE);
    display.setCursor(15, 15);
    display.println("Sending");
    display.display();
    delay(20);
  }
  SendText("Sent");
}


void HelpText(String txt) {
  display.setTextSize(1); // Draw 1X-scale text
  display.setTextColor(WHITE);
  display.setCursor(4, 18);
  display.println(txt);
  display.display();
}


void BatLeval(int Batt) {
  
  if(Batt > 85) {
    display.drawPixel(119, 2, WHITE);
    display.drawPixel(119, 3, WHITE);
  }

  if(Batt > 70) {
    display.drawPixel(120, 2, WHITE);
    display.drawPixel(120, 3, WHITE);
  }

  if(Batt > 55) {
    display.drawPixel(121, 2, WHITE);
    display.drawPixel(121, 3, WHITE);
  }

  if(Batt > 40) {
    display.drawPixel(122, 2, WHITE);
    display.drawPixel(122, 3, WHITE);
  }

  if(Batt > 25) {
    display.drawPixel(123, 2, WHITE);
    display.drawPixel(123, 3, WHITE);
  }
  
  if(Batt > 20 | random(1,3) > 1){
    display.drawRect(118, 1, 7, 4, WHITE);
    display.drawPixel(117, 3, WHITE);
    display.drawPixel(117, 2, WHITE);
  }
  
  display.display();
  delay(25);
}

void GoDot(void) {
  if ((micros() % 2) == 0) {
      display.drawPixel(1,1,WHITE);
      display.display();
  } else {
      display.drawPixel(1,1,BLACK);
      display.display();
  }
  delay(25);

  if(Button(buttonTake) & Button(buttonSend)) {
    Settings();
  }
}


bool Button(int N) {
  int buttonState = 0;
  bool btn = false;
  
  buttonState = digitalRead(N);
  if (buttonState == HIGH) {
    btn = false;
  } else {
    btn = true;
  }
  delay(100);
  return btn;
}


void drawSun(void) {
  display.clearDisplay();
  for(int i=0; i<anoPins; i++){
    lightVals[i] = getLight(lightpins[i]);

    if(Count < nomean) {
      lightMeans[i] += lightVals[i];
    } else {
      lightMeans[i] = lightVals[i];
    }
    
    if(verbosemode) {
      Serial.print(LighttoPAR(lightVals[i])); Serial.print(":PAR,"); 
      Serial.print(lightVals[i]);  Serial.print(":Raw,");
    }      
        
    display.drawLine(map(i, 0, 6, 1, display.width()), display.height()-map(lightVals[i], 0, 3000, 1,display.height()), map(i+1, 0, 6, 1, display.width()), display.height()-map(lightVals[i], 0, 3000, 1,display.height()), WHITE);
  }

  if(verbosemode) {
    Serial.print("MeanRaw:");
    Serial.print(Avr(lightVals, anoPins));
    Serial.print(",MeanPAR:");
    Serial.print(LighttoPAR(Avr(lightVals, anoPins)));
  }  

  if(Count < nomean) {
    Count++;
  } else {
    Count = 1;
  }
  
  display.display(); // Update screen with each newly-drawn line
  delay(20);
}



// Adafruit functions
void startAdv(void)
{  
  // Advertising packet
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addTxPower();
  Bluefruit.Advertising.addAppearance(BLE_APPEARANCE_HID_KEYBOARD);
  
  // Include BLE HID service
  Bluefruit.Advertising.addService(blehid);

  // There is enough room for the dev name in the advertising packet
  Bluefruit.Advertising.addName();
  
  /* Start Advertising
   * - Enable auto advertising if disconnected
   * - Interval:  fast mode = 20 ms, slow mode = 152.5 ms
   * - Timeout for fast mode is 30 seconds
   * - Start(timeout) with timeout = 0 will advertise forever (until connected)
   * 
   * For recommended advertising interval
   * https://developer.apple.com/library/content/qa/qa1931/_index.html   
   */
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setInterval(32, 244);    // in unit of 0.625 ms
  Bluefruit.Advertising.setFastTimeout(30);      // number of seconds in fast mode
  Bluefruit.Advertising.start(0);                // 0 = Don't stop advertising after n seconds
}

void set_keyboard_led(uint16_t conn_handle, uint8_t led_bitmap)
{
  (void) conn_handle;
  
  // light up Red Led if any bits is set
  if ( led_bitmap )
  {
    ledOn( LED_RED );
  }
  else
  {
    ledOff( LED_RED );
  }
}

float readVBAT(void) {
  float raw;
 
  // Set the analog reference to 3.0V (default = 3.6V)
  analogReference(AR_INTERNAL_3_0);
 
  // Set the resolution to 12-bit (0..4095)
  analogReadResolution(12); // Can be 8, 10, 12 or 14
 
  // Let the ADC settle
  delay(1);
 
  // Get the raw 12-bit, 0..3000mV ADC value
  raw = analogRead(vbat_pin);
 
  // Set the ADC back to the default settings
  analogReference(AR_DEFAULT);
  analogReadResolution(10);
 
  // Convert the raw value to compensated mv, taking the resistor-
  // divider into account (providing the actual LIPO voltage)
  // ADC range is 0..3000mV and resolution is 12-bit (0..4095)
  return raw * REAL_VBAT_MV_PER_LSB;
}
 
uint8_t mvToPercent(float mvolts) {
  if(mvolts<3300)
    return 0;
 
  if(mvolts <3600) {
    mvolts -= 3300;
    return mvolts/30;
  }
 
  mvolts -= 3600;
  return 10 + (mvolts * 0.15F );  // thats mvolts /6.66666666
}


void Settings(void) {
    NeoSet(true, 102, 0, 102);
    
    SendText(Version);
    HelpText("S0 dga1@aber.ac.uk");
    Serial.print("For errors contact dga1@aber.ac.uk");
    Serial.println(Version);
    delay(500);

    if(Button(buttonTake) & Button(buttonSend)) {
      delay(1000);

      SendText("Settings");
      HelpText("See manual for help");
      delay(1000);
      
      int x = 0;
      while (x == 0) {
        takebtn = Button(buttonTake);
        sendbtn = Button(buttonSend);
        
        if(sendbtn) {
          if(verbosemode) {
            verbosemode = false;
          } else {
            verbosemode = true;
          }
        }
        
        if(verbosemode) {
          SendText("Verbose");
        } else {
          SendText("Succinct");
        }
        delay(200);
        
        if(takebtn) {
          x = 1;
        }
      }
      
      while (x == 1) {
        takebtn = Button(buttonTake);
        sendbtn = Button(buttonSend);
        
        if(sendbtn) {
          if(sendall) {
            sendall = false;
          } else {
            sendall = true;
          }
        }
        
        if(sendall) {
          SendText("6 values");
          HelpText("Sent by BT");
        } else {
          SendText("1 value");
          HelpText("Sent by BT");
        }
        delay(200);
        
        if(takebtn) {
          x = 2;
        }
      }
      
      while (x == 2) {
        takebtn = Button(buttonTake);
        sendbtn = Button(buttonSend);
        
        if(sendbtn) {
          nomean++;
          if(nomean > 100) {
            nomean = 1;
          }
        }
        
        SendText(String(nomean, DEC));
        HelpText("Readings to mean");
        delay(200);
        
        if(takebtn) {
          x = 3;
        }
      }

      while (x == 3) {
        takebtn = Button(buttonTake);
        sendbtn = Button(buttonSend);
        
        if(sendbtn) {
          LEuse++;
          if(LEuse > 4) {
            LEuse = 0;
          }
        }

        if(LEuse==0) {
          SendText("None");
        }
        if(LEuse==1) {
          SendText("Space");
        }
        if(LEuse==2) {
          SendText("Comma");
        }
        if(LEuse==3) {
          SendText("Tab");
        }
        if(LEuse==4) {
          SendText("New Line");
        }
        HelpText("Line end");
        delay(200);
        
        if(takebtn) {
          x = 4;
        }
      }

      while (x == 4) {
        takebtn = Button(buttonTake);
        sendbtn = Button(buttonSend);
        
        if(sendbtn) {
          if(slowmode) {
            slowmode = false;
          } else {
            slowmode = true;
          }
        }
        
        if(slowmode) {
          SendText("Calibration");
          HelpText("Works at 30s!");
        } else {
          SendText("Normal");
          HelpText("Works at 0.4s");
        }
        delay(200);
        
        if(takebtn) {
          x = 5;
        }
      }

      
    }
    NeoSet(false, 0,0,0);
}


void Sound(int ms, int freq) {
  delay(ms);
  tone(beep, freq, ms);
}


int LighttoPAR(int MeanRead) {
  int PAR;
  PAR = (MeanRead * 1.408)+ 0.49;

  return PAR;
}


void NeoSet(bool onoff, int R, int G, int B){
    if(onoff){
      pixels.clear(); 
      pixels.setPixelColor(0, pixels.Color(R, G, B));
      pixels.show();
    } else {
      pixels.clear();
      pixels.show();
    }
}
